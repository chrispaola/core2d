//
// Vector2d.h - Definition of a basic 2D vector class.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//

#ifndef VECTOR2D_H
#define VECTOR2D_H

#include <math.h>
#include <assert.h>
#include "Defs2d.h"

class Vector2d
{
public:
	// Construction
	Vector2d(float x = 0.0, float y = 0.0);
	Vector2d(const float v[]);
	Vector2d(const Vector2d& v);
	const Vector2d& operator=(const Vector2d& v);
	const Vector2d operator+(const Vector2d& v);
	const Vector2d operator-(const Vector2d& v);
	const Vector2d operator/(float fDivisor);
	const Vector2d operator*(float fMult);
	bool operator==(const Vector2d& v);
	bool operator!=(const Vector2d& v);

	// Operations
	void Set(float x, float y);
	float Dot(const Vector2d& v) const;
	float Cross(const Vector2d& v) const;
	float Length() const; 
	float Distance(Vector2d& v) const;
	void Unitize();
	float Angle(const Vector2d& v) const;

	// Data members 
	float m_x, m_y;
};

#include "Vector2d.inl"

#endif
