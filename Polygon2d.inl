//
// Polygon2d.inl - Implementation of inline methods of the Polygon2d class.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//

#include <vector>
#include <assert.h>
#include <limits.h>

// Default constructor.
inline
Polygon2d::Polygon2d()
{

}

// Copy constructor.
inline
Polygon2d::Polygon2d(const Polygon2d& polygon)
{
	m_List = polygon.m_List;
}

// Construct a polygon from an array of vectors.
inline
Polygon2d::Polygon2d(const Vector2d vertices[], Uint nVertexCount)
{
	Uint i = 0;

	while(i < nVertexCount)
		m_List.push_back(vertices[i++]);
}

// Destructor.
inline
Polygon2d::~Polygon2d()
{

}

// Overloaded assignment operator.
inline
const Polygon2d& Polygon2d::operator=(const Polygon2d& polygon)
{
	m_List = polygon.m_List;
	return *this;
}

inline
bool Polygon2d::operator<(const Polygon2d& polygon)
{
	return (GetLeftness() < polygon.GetLeftness());
}



// Return an index suitable for indexing the vector list.  The input index
// can be any number, positive or negative, and the output index is guaranteed
// to lie in the range [0, n) where n is the number of vertices.  If the list 
// is empty, -1 is returned.
inline
Uint Polygon2d::GetIndexInBounds(int nIndex) const
{
	int nPos	= nIndex;
	int nIncr	= (nIndex > 0) ? -m_List.size() : m_List.size();

	// No points in polygon!
	if (m_List.size() == 0)
		return -1;

	while(!(0 <= nPos && nPos < m_List.size()))
		nPos += nIncr;

	return nPos;
}

// Return a vertex based on its index.  The index is taken modulo n (where 
// n is the number of vertices), therefore the index can be any number, 
// positive or negative.
inline 
const Vector2d&	Polygon2d::GetVertex(int nIndex) const
{
	return m_List[GetIndexInBounds(nIndex)];
}

// Remove a vertex based on its index.  The index is taken modulo n (where 
// n is the number of vertices), therefore the index can be any number, 
// positive or negative.
inline
void Polygon2d::RemoveVertex(int nIndex)
{
	m_List.erase(m_List.begin() + GetIndexInBounds(nIndex));
}

// Remove all vertices in the polygon.
inline
void Polygon2d::RemoveAllVertices()
{
	m_List.clear();
}

// Add a point to a polygon.
inline
void Polygon2d::AddVertex(const Vector2d& vector)
{
	m_List.push_back(vector);
}

inline
void Polygon2d::AddVertex(float x, float y)
{
	m_List.push_back(Vector2d(x, y));
}

// Return the number of points in this polygon.
inline
Uint Polygon2d::GetVertexCount() const
{
	return m_List.size();
}

// Returns true if a given vertex is convex.  If the vertex is reflex, 
// false is returned.
inline
bool Polygon2d::IsVertexConvex(Uint nIndex) const
{
	Vector2d v0 = GetVertex(nIndex - 1);
	Vector2d v1 = GetVertex(nIndex);
	Vector2d v2 = GetVertex(nIndex + 1);
	
	return(
			(v1.m_x - v0.m_x) * (v2.m_y - v1.m_y) - 
			(v1.m_y - v0.m_y) * (v2.m_x - v1.m_x)
	) < 0;
}

// Returns true if the winding order of the polygon is counter-clockwise, or
// false otherwise.
inline
bool Polygon2d::IsCCW() const
{
	return (GetArea() < 0.0);
}
