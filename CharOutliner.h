//
// CharOutliner.h - Definition of CharOutliner, a class to produce 
// character outlines.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//
#ifndef CHAROUTLINER_H
#define CHAROUTLINER_H

#include <windows.h>
#include "Defs2d.h"
#include "Polygon2d.h"
#include "Char.h"

class CharOutliner
{
public:
	// Construction
	CharOutliner();
	~CharOutliner();

	// Operations
	bool Init(LOGFONT* pLogFont, Uint nSplineQuality = 2);
	bool GetOutline(UINT nChar, Char* pChar);
	bool GetOutline(UINT nStartChar, UINT nEndChar, Char* pChar);

protected:
	bool GetOutlineBuffer(UINT nChar, LPVOID* ppBuffer, DWORD* pdwBufferSize, 
							LPGLYPHMETRICS pGlyphMetrics);
	void GetPolygons(LPVOID pBuffer, DWORD dwBufferSize, LPGLYPHMETRICS pGlyphMetrics,
							PolygonList& polyVect);

private:
	// Member data
	HDC		m_hDC;				// Memory device context
	HFONT	m_hPrevFont;		// Previous font selected in DC
	HFONT	m_hFont;			// New font to select in DC
	Uint	m_nSplineQuality;	// Spline approximation quality (higher -> better quality)
};

#endif