//
// Char.cpp - Definition of the Char class.
//
// The Char class stores an outline of the character, a triangle 
// decomposition of the character, and other fields useful for typesetting.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//
#include <assert.h>
#include "Char.h"

Char::Char()
{

}

Char::~Char()
{
}

const Polygon2d* Char::GetPolygon(Uint nIndex)
{
	assert(nIndex < m_PolygonList.size());
	return &m_PolygonList.at(nIndex);
}

const Triangle* Char::GetTriangle(Uint nIndex)
{
	assert(nIndex < m_TriangleList.size());
	return &m_TriangleList.at(nIndex);
}

