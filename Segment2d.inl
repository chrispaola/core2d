//
// Segment2d.inl - Implementation of inline methods of the Segment2d class.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//

// Constructs a segment from two coordinates, (x0,y0) and (x1,y1)
inline
Segment2d::Segment2d(float x0, float y0, float x1, float y1)
{
	m_v0.Set(x0, y0);
	m_v1.Set(x1, y1);
}

// Constructs a segment from two vectors.
inline
Segment2d::Segment2d(const Vector2d& v0, const Vector2d& v1)
	: m_v0(v0), m_v1(v1)
{
}

// Constructs a segment from an array of two vectors.
inline
Segment2d::Segment2d(const Vector2d v[])
	: m_v0(v[0]), m_v1(v[1])
{
}

// Copy constructor.
inline
Segment2d::Segment2d(const Segment2d& segment)
{
	m_v0 = segment.m_v0;
	m_v1 = segment.m_v1;
}

// Assignment operator.
inline
const Segment2d& Segment2d::operator=(const Segment2d& segment)
{
	m_v0 = segment.m_v0;
	m_v1 = segment.m_v1;
	return *this;
}

// Returns TRUE if the segment intersects with the given segment, and sets
// the intersection point p.  If the segments do not intersect, FALSE is
// returned.
inline
bool Segment2d::Intersects(const Segment2d& segment, Vector2d& p) const
{
	float t0			= 0.0f;
	float t1			= 0.0f;
	float fNumerator0	= 0.0f;
	float fNumerator1	= 0.0f;
	float fDenominator	= 0.0f;

	fDenominator
	=
	(segment.m_v1.m_y - segment.m_v0.m_y) * (m_v1.m_x - m_v0.m_x)
	-
	(segment.m_v1.m_x - segment.m_v0.m_x) * (m_v1.m_y - m_v0.m_y);

	if (fDenominator == 0.0)
	{
		// The line segments are parallel, don't continue.
		return false;
	}

	fNumerator0
	=
	(segment.m_v1.m_x - segment.m_v0.m_x) * (m_v0.m_y - segment.m_v0.m_y)
	-
	(segment.m_v1.m_y - segment.m_v0.m_y) * (m_v0.m_x - segment.m_v0.m_x);

	fNumerator1
	=
	(m_v1.m_x - m_v0.m_x) * (m_v0.m_y - segment.m_v0.m_y)
	-
	(m_v1.m_y - m_v0.m_y) * (m_v0.m_x - segment.m_v0.m_x);

	t0 = fNumerator0 / fDenominator;
	t1 = fNumerator1 / fDenominator;

	if ((0.0 <= t0 && t0 <= 1.0) && (0.0 <= t1 && t1 <= 1.0))
	{
		// We're in business... the segments intersect.  Calculate the
		// point of intersection.
		p.m_x = m_v0.m_x + t0 * (m_v1.m_x - m_v0.m_x);
		p.m_y = m_v0.m_y + t0 * (m_v1.m_y - m_v0.m_y);

		return true;
	}

	return false;
}

// Returns TRUE if the slope of the segment is defined (i.e. the segment is 
// not vertical.  When TRUE is returned, the slope is returned through the 
// reference fSlope.  If the slope of not defined, FALSE is returned.
inline
bool Segment2d::Slope(float& fSlope) const
{	
	float dy = m_v1.m_y - m_v0.m_y;
	float dx = m_v1.m_x - m_v0.m_x;

	if (dx == 0.0)
		return false;
	
	fSlope = dy / dx;
	return true;
}

// Returns true if both given points lie on the same side of the segment,
// false is returned otherwise.
inline
bool Segment2d::SameSide(const Vector2d& v0, const Vector2d& v1) const
{
	return(
		((v0.m_x - m_v0.m_x) * (m_v1.m_y - m_v0.m_y) - 
			(m_v1.m_x - m_v0.m_x) * (v0.m_y - m_v0.m_y)) * 
		((v1.m_x - m_v0.m_x) * (m_v1.m_y - m_v0.m_y) - 
			(m_v1.m_x - m_v0.m_x) * (v1.m_y - m_v0.m_y))
	) >= 0.0;
//	float fCross0 = m_v0.Cross(v0);
//	float fCross1 = m_v0.Cross(v1);

//	if (fCross0 > 0.0)
//		return (fCross1 > 0.0);
//	else if (fCross0 < 0.0)
//		return (fCross1 < 0.0);
//	else
//		return (fCross1 == 0.0);
}

// Returns a vector that is perpendicular to this segment.
inline
Vector2d Segment2d::Normal() const
{
	float fSlope = 0.0f;

	if (!Slope(fSlope))
	{
		// Line is vertical
		return Vector2d(1.0, 0.0);
	}
	else
	{
		if (fSlope == 0.0)
		{
			// Line is horizontal
			return Vector2d(0.0, 1.0);
		}
	}

	// Line is neither vertical or horizontal
	return Vector2d(1.0, -1.0f / fSlope);
}

// Returns the length of the segment.
inline
float Segment2d::Length() const
{
	float fDeltaX = m_v1.m_x - m_v0.m_x;
	float fDeltaY = m_v1.m_y - m_v0.m_y;

	return (float)sqrt((fDeltaX * fDeltaX) + (fDeltaY * fDeltaY));
}


// Returns the distance of a vector to this segment.
inline
float Segment2d::Distance(const Vector2d& v) const
{
	float distance = FLT_MAX;
	float length = Length();
	float u = (((v.m_x - m_v0.m_x) * (m_v1.m_x - m_v0.m_x)) + ((v.m_y - m_v0.m_y) * (m_v1.m_y - m_v0.m_y))) 
				/ (length * length);

	if (0.0f <= u && u <= 1.0f)
	{
		Vector2d	i;
		Segment2d	proj;
		
		i.m_x = m_v0.m_x + u * (m_v1.m_x - m_v0.m_x);
		i.m_y = m_v0.m_y + u * (m_v1.m_y - m_v0.m_y);

		proj.m_v0 = v;
		proj.m_v1 = i;

		distance = proj.Length();
	}

	return distance;
}

inline
bool Segment2d::operator==(const Segment2d& segment)
{
	return (m_v0 == segment.m_v0) && (m_v1 == segment.m_v1);
}
