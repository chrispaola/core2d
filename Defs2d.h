//
// Defs2d.h - Definition of basic types for the Core2d library.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//

#ifndef DEFS2D_H
#define DEFS2D_H

// Shorter names for convenience
typedef unsigned int Uint;

#include <float.h>

#ifndef M_PI
#define M_PI (3.141592654)
#endif

#ifndef FLOAT_TOLERANCE
#define FLOAT_TOLERANCE		(1.0e-4)
#endif

struct Rect
{
	int x1;
	int y1;
	int x2;
	int y2;
};

#endif