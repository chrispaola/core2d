//
// Rect2d.inl - Implementation of inline methods of the Rect2d class.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//	

// Constructs a rect from four floats.
inline
Rect2d::Rect2d(float x0 /*= 0.0*/, float y0 /*= 0.0*/, float x1 /*= 0.0*/, float y1 /*= 0.0*/) 
	: m_x0(x0), m_y0(y0), m_x1(x1), m_y1(y1)
{
	Normalize();
}

// Constructs a rect from an array of four floats.
inline
Rect2d::Rect2d(const float rect[])
	: m_x0(rect[0]), m_y0(rect[1]), m_x1(rect[2]), m_y1(rect[3])
{
	Normalize();
}

// Copy constructor.
inline
Rect2d::Rect2d(const Rect2d& rect)
	: m_x0(rect.m_x0), m_y0(rect.m_y0), m_x1(rect.m_x1), m_y1(rect.m_y1)
{
	Normalize();
}

// Constructs a rect from two vectors
inline
Rect2d::Rect2d(const Vector2d& v0, const Vector2d& v1)
{
	m_x0 = v0.m_x;
	m_y0 = v0.m_y;
	m_x1 = v1.m_x;
	m_y1 = v1.m_y;
	Normalize();
}

// Assignment operator.
inline
const Rect2d& Rect2d::operator=(const Rect2d& rect)
{
	m_x0 = rect.m_x0;
	m_y0 = rect.m_y0;
	m_x1 = rect.m_x1;
	m_y1 = rect.m_y1;
	Normalize();

	return *this;
}

// Set components.
inline
void Rect2d::Set(float x0, float y0, float x1, float y1)
{
	m_x0 = x0;
	m_y0 = y0;
	m_x1 = x1;
	m_y1 = y1;
	Normalize();
}

// Set this rect to the union of this rect and the given rect.
inline
void Rect2d::Union(const Rect2d& rect)
{
	m_x0 = min(m_x0, rect.m_x0);
	m_y0 = min(m_y0, rect.m_y0);
	m_x1 = max(m_x1, rect.m_x1);
	m_y1 = max(m_y1, rect.m_y1);
}

// Set this rect to the intersection of this rect and the given rect.
inline
void Rect2d::Intersect(const Rect2d& rect)
{
	m_x0 = max(m_x0, rect.m_x0);
	m_y0 = max(m_y0, rect.m_y0);
	m_x1 = min(m_x1, rect.m_x1);
	m_y1 = min(m_y1, rect.m_y1);
}

// Test whether the given point is inside the rect.
inline
bool Rect2d::IsPointInside(const Vector2d& v)
{
	return (m_x0 <= v.m_x && v.m_x <= m_x1) && (m_y0 <= v.m_y && v.m_y <= m_y1);
}

// Test whether the given segment is inside the rect.
inline
bool Rect2d::IsSegmentInside(const Segment2d& segment)
{
	return IsPointInside(segment.m_v0) && IsPointInside(segment.m_v1);
}

inline
float Rect2d::Width() const
{
	return m_x1 - m_x0;
}

inline
float Rect2d::Height() const
{
	return m_y1 - m_y0;
}

inline
bool Rect2d::IsEmpty() const
{
	return (fabs(m_x1 - m_x0) < FLOAT_TOLERANCE) && 
		(fabs(m_y1 - m_y0) < FLOAT_TOLERANCE);
}

inline
void Rect2d::Normalize()
{
	if (m_x1 < m_x0)
		swap(m_x0, m_x1);

	if (m_y1 < m_y0)
		swap(m_y0, m_y1);
}
