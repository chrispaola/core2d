//
// Polygon2d.h - Definition of a basic Polygon2d class.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//
#ifndef POLYGON_H
#define POLYGON_H

#include <vector>
#include "Defs2d.h"
#include "Vector2d.h"
#include "Triangle.h"

class Polygon2d
{
	// Construction
public:
	Polygon2d();
	Polygon2d(const Polygon2d& polygon);
	Polygon2d(const Vector2d vertices[], Uint nVectexCount);
	~Polygon2d();

	// Operators
	// Assignment operator.
	const Polygon2d& operator=(const Polygon2d& polygon);
	bool operator<(const Polygon2d& polygon);

	// Operations
	const Vector2d&	GetVertex(int nIndex) const;
	void			SetVertex(int nIndex, const Vector2d& vector);
	void			RemoveVertex(int nIndex);
	void			RemoveAllVertices();
	void			AddVertex(const Vector2d& vector);
	void			AddVertex(float x, float y);
	Uint			GetVertexCount() const;
	bool			IsVertexConvex(Uint nIndex) const;
	Uint			GetIndexInBounds(int nIndex) const;
	float			GetArea() const;
	float			GetLeftness() const;
	bool			IsPointInside(const Vector2d& point) const;
	bool			IsPolygonInside(const Polygon2d& polygon) const;
	bool			IsCCW() const;
	void			Reverse();
	void			MergePolygon(Polygon2d& polygon);
	void			Triangulate(TriangleList& triList) const;
	void			RemoveDuplicates();
	void			GetBoundingBox(float& x0, float& y0, float& x1, float& y1);
	void			Translate(float dx, float dy);

protected:
	// Operations
	Uint GetNextEar() const;

protected:
	// Data types
	typedef std::vector<Vector2d> List;

	// Data members
	List m_List;
};

typedef std::vector<Polygon2d> PolygonList;

#include "Polygon2d.inl"

#endif