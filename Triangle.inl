//
// Triangle.inl - Implementation of inline methods for the Triangle class.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//

#include "Segment2d.h"

// Default constructor
inline
Triangle::Triangle()
{
}

// Construct a triangle from three points
inline
Triangle::Triangle(const Vector2d& a, const Vector2d& b, const Vector2d& c)
	: m_a(a), m_b(b), m_c(c)
{
}

// Construct a triangle from three sets of coordinates
inline
Triangle::Triangle(float x0, float y0, float x1, float y1, float x2, float y2)
	: m_a(x0, y0), m_b(x1, y1), m_c(x2, y2)
{
}

// Copy constructor
inline
Triangle::Triangle(const Triangle& tri)
	: m_a(tri.m_a), m_b(tri.m_b), m_c(tri.m_c)
{	
}

// Assignment operator
inline
const Triangle& Triangle::operator=(const Triangle& tri)
{
	m_a = tri.m_a;
	m_b = tri.m_b;
	m_c = tri.m_c;

	return *this;
}

// Set the vertices for this triangle
inline
void Triangle::Set(const Vector2d& a, const Vector2d& b, const Vector2d& c)
{
	m_a = a;
	m_b = b;
	m_c = c;
}

// Set the vertices for this triangle
inline
void Triangle::Set(float x0, float y0, float x1, float y1, float x2, float y2)
{
	m_a.Set(x0, y0);
	m_b.Set(x1, y1);
	m_c.Set(x2, y2);
}

// Get the vertices for this triangle
inline
void Triangle::Get(Vector2d& a, Vector2d b, Vector2d c) const
{
	a = m_a;
	b = m_b;
}

// Get the vertices for this triangle
inline
void Triangle::Get(float& x0, float& y0, float& x1, float& y1, float& x2, float& y2)
{
	x0 = m_a.m_x;
	y0 = m_a.m_y;

	x1 = m_b.m_x;
	y1 = m_b.m_y;
	
	x2 = m_c.m_x;
	y2 = m_c.m_y;

}

// Returns true if the given point lies inside or on the border of the 
// triangle.  False is returned otherwise.
inline
bool Triangle::IsPointInside(const Vector2d& point) const
{
	Segment2d ab(m_a, m_b);		// Segment from a to b
	Segment2d bc(m_b, m_c);		// Segment from b to c
	Segment2d ca(m_c, m_a);		// Segment from c to a

	return(
		ab.SameSide(point, m_c) && 
		bc.SameSide(point, m_a) && 
		ca.SameSide(point, m_b)
	);
}