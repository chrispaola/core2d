//
// Segment2d.h - Definition of a basic 2D segment class.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//
#ifndef SEGMENT2D_H
#define SEGMENT2D_H

#include "Vector2d.h"

class Segment2d
{
public:
	// Construction
	Segment2d(float x0 = 0.0, float y0 = 0.0, float x1 = 0.0, float y1 = 0.0);
	Segment2d(const Vector2d& v0, const Vector2d& v1);
	Segment2d(const Vector2d v[]);
	Segment2d(const Segment2d& segment);

	bool Segment2d::operator==(const Segment2d& segment);

	// Operations
	const Segment2d& operator=(const Segment2d& segment);
	bool Intersects(const Segment2d& segment, Vector2d& p) const;
	bool Slope(float& fSlope) const;
	bool SameSide(const Vector2d& v0, const Vector2d& v1) const;
	Vector2d Normal() const;
	float Length() const;
	float Distance(const Vector2d& v) const;

	// Data members
	Vector2d m_v0;
	Vector2d m_v1;
};

#include "Segment2d.inl"

#endif