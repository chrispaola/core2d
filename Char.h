//
// Char.h - Definition of the Char class.
//
// The Char class stores an outline of the character, a triangle 
// decomposition of the character, and other fields useful for typesetting.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//
#ifndef CHAR_H
#define CHAR_H

#include "Defs2d.h"
#include "Vector2d.h"
#include "Triangle.h"
#include "Polygon2d.h"

class Char
{
public:
	// Constructors / Destructor
	Char();
	~Char();

	Uint GetPolygonCount() { return m_PolygonList.size(); }
	Uint GetTriangleCount() { return m_TriangleList.size(); }
	Rect GetBoundingBox() { return m_BoundingBox; }
	Vector2d GetCellInc() { return m_CellInc; }

	const Polygon2d* GetPolygon(Uint nIndex);
	const Triangle* GetTriangle(Uint nIndex);

	// Data members
public:
	PolygonList		m_PolygonList;	// List of polygonal outlines
	TriangleList	m_TriangleList;	// List of triangles from decomposition
	Rect			m_BoundingBox;	// Char bounding box
	Vector2d		m_CellInc;		// Distance from current character cell to next
};

#endif