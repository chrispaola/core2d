//
// Triangle.h - Definition of a triangle class.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//
#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <vector>
#include "Vector2d.h"

class Triangle
{
public:
	// Construction
	Triangle();
	Triangle(const Vector2d& a, const Vector2d& b, const Vector2d& c);
	Triangle(float x0, float y0, float x1, float y1, float x2, float y2);
	Triangle(const Triangle& tri);

	// Operators
	const Triangle& operator=(const Triangle& tri);

	// Operations
	void Set(const Vector2d& a, const Vector2d& b, const Vector2d& c);
	void Set(float x0, float y0, float x1, float y1, float x2, float y2);
	void Get(Vector2d& a, Vector2d b, Vector2d c) const;
	void Get(float& x0, float& y0, float& x1, float& y1, float& x2, float& y2);
	bool IsPointInside(const Vector2d& point) const;

	// Data members
	Vector2d m_a, m_b, m_c;

};

typedef std::vector<Triangle> TriangleList;

#include "Triangle.inl"

#endif