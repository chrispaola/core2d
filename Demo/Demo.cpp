// Demo.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Demo.h"
#include "../CharOutliner.h"
#include "../Char.h"

#define MAX_LOADSTRING 100

HWND g_hwnd = NULL;

int g_LastX = 0;
int g_LastY = 0;
int g_tx = 0;
int g_ty = 0;
bool g_IsMouseDown = false;

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in thig_ code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_DEMO, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_DEMO);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_DEMO);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= (LPCTSTR)IDC_DEMO;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }
   g_hwnd = hWnd;

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

Char g_chars[255];
int g_charCount = sizeof(g_chars) / sizeof(g_chars[0]);

void OnCreate() 
{
	CharOutliner outliner;
	CHOOSEFONT chooseFont = { sizeof(CHOOSEFONT) };
	LOGFONT logFont = { sizeof(LOGFONT) };

	chooseFont.Flags = CF_SCREENFONTS;
	chooseFont.lpLogFont = &logFont;

	if (::ChooseFont(&chooseFont)) 
	{
		if (outliner.Init(&logFont)) {

			for (int i = 0; i < g_charCount; i++)
			{
				outliner.GetOutline(i, &g_chars[i]);
			}
		}
	}	
}

void DrawTriangle(HDC hdc, int x, int y, const Triangle *pTriangle)
{
	x += g_tx;
	y += g_ty;

	// a -> b
	MoveToEx(hdc, x + pTriangle->m_a.m_x, y + pTriangle->m_a.m_y, NULL);
	LineTo(hdc, x + pTriangle->m_b.m_x, y + pTriangle->m_b.m_y);

	// b -> c
	MoveToEx(hdc, x + pTriangle->m_b.m_x, y + pTriangle->m_b.m_y, NULL);
	LineTo(hdc, x + pTriangle->m_c.m_x, y + pTriangle->m_c.m_y);

	// c -> a
	MoveToEx(hdc, x + pTriangle->m_c.m_x, y + pTriangle->m_c.m_y, NULL);
	LineTo(hdc, x + pTriangle->m_a.m_x, y + pTriangle->m_a.m_y);
}

void DrawPolygon(HDC hdc, int x, int y, const Polygon2d *pPoly)
{
	x += g_tx;
	y += g_ty;

	int vertexCount = pPoly->GetVertexCount();
	if (vertexCount > 0) 
	{
		Vector2d firstVertex = pPoly->GetVertex(0);
		MoveToEx(hdc, firstVertex.m_x + x, firstVertex.m_y + y, NULL);

		for (int i = 1; i < vertexCount; i++)
		{
			Vector2d vertex = pPoly->GetVertex(i);
			LineTo(hdc, vertex.m_x + x, vertex.m_y + y);
			MoveToEx(hdc, vertex.m_x + x, vertex.m_y + y, NULL);
		}

		LineTo(hdc, firstVertex.m_x + x, firstVertex.m_y + y);
	} 

}

void OnPaint(HDC hdc)
{
	float currX = 0;
	float currY = 100;

	for (int i = 0; i < g_charCount; i++)
	{
		Char character = g_chars[i];

		//
		// Draw triangles
		// 
		int triangleCount = character.GetTriangleCount();
		for (int j = 0; j < triangleCount; j++)
		{
			const Triangle *pTriangle = character.GetTriangle(j);
			DrawTriangle(hdc, currX, currY, pTriangle);
		}
		Rect boundingBox = character.GetBoundingBox();
		int height = boundingBox.y2 - boundingBox.y1;

		int polyCount = character.GetPolygonCount();
		for (int j = 0; j < polyCount; j++) 
		{
			const Polygon2d *pPoly = character.GetPolygon(j);
			DrawPolygon(hdc, currX, currY + height + 50, pPoly);
		}

		currX += character.GetCellInc().m_x;
		currY += character.GetCellInc().m_y;
	}


}

void OnLButtonDown(int x, int y)
{
	g_IsMouseDown = true;
	g_LastX = x;
	g_LastY = y;
}

void OnMouseMove(int x, int y)
{
	if (g_IsMouseDown) 
	{
		int deltaX = x - g_LastX;
		int deltaY = y - g_LastY;

		g_tx += deltaX;
		g_ty += deltaY;

		g_LastX = x;
		g_LastY = y;

		InvalidateRect(g_hwnd, NULL, TRUE);
	}
}

void OnLButtonUp(int x, int y)
{
	g_IsMouseDown = false;
}


//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message) 
	{
	case WM_CREATE:
		OnCreate();
		break;

	case WM_COMMAND:
		wmId    = LOWORD(wParam); 
		wmEvent = HIWORD(wParam); 
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		OnPaint(hdc);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_LBUTTONDOWN:
		OnLButtonDown(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_MOUSEMOVE:
		OnMouseMove(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_LBUTTONUP:
		OnLButtonUp(LOWORD(lParam), HIWORD(lParam));
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
		{
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	}
	return FALSE;
}
