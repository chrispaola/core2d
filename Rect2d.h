//
// Rect2d.h - Definition of a basic 2D rectangle class.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//
#ifndef RECT2D_H
#define RECT2D_H

#include <math.h>
#include <assert.h>
#include "Defs2d.h"

class Rect2d
{
public:
	// Construction
	Rect2d(float x0 = 0.0, float y0 = 0.0, float x1 = 0.0, float y1 = 0.0);
	Rect2d(const Vector2d& v0, const Vector2d& v1);
	Rect2d(const float rect[]);
	Rect2d(const Rect2d& rect);
	const Rect2d& operator=(const Rect2d& rect);

	// Operations
	void Set(float x0, float y0, float x1, float y1);
	void Union(const Rect2d& rect);
	void Intersect(const Rect2d& rect);
	bool IsPointInside(const Vector2d& v);
	bool IsSegmentInside(const Segment2d& segment);
	void Normalize();
	float Width() const;
	float Height() const;
	bool IsEmpty() const;

	// Data members 
	float m_x0, m_y0, m_x1, m_y1;
};

#include "Rect2d.inl"

#endif
