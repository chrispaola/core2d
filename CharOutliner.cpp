//
// CharOutliner.cpp - Implementation of CharOutliner, a class to produce 
// glyph outlines.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//

#include <vector>
#include <algorithm>
#include "CharOutliner.h"

// Begin debug
#include <windows.h>
#include <stdio.h>
// End debug

////////////////////////////////////////////////////////////////////////////////
//
// Helper routines used by this class.
//
////////////////////////////////////////////////////////////////////////////////

// FloatToFixed - Convert a float to FIXED.
static 
FIXED FloatToFixed(float f)
{
	FIXED fixed;

	fixed.value = (short)f;
	fixed.fract = (WORD)((f - (short)f)/65535.0);

	return fixed;
}

// FixedToFloat - Convert a FIXED to float.
static
float FixedToFloat(FIXED* pFixed)
{
	assert(pFixed != NULL);
	return pFixed->value + pFixed->fract / 65535.0;
}

// PointFXToVector2d - Convert a POINTFX to Vector2d.
static
Vector2d PointFXToVector2d(POINTFX* pPoint)
{
	assert(pPoint != NULL);

	return Vector2d(FixedToFloat(&pPoint->x), FixedToFloat(&pPoint->y));
}

// AddPolyLineToPolygon - Add an array of POINTFX to a polygon.
// 
// Parameters:
//	pPolygon		- Polygon to add points to.
//	rgPointFX		- Array of points.
//	wPointCount		- Number of points in array.
static
void AddPolyLineToPolygon(Polygon2d* pPolygon, POINTFX* rgPointFX, WORD wPointCount)
{
	assert(pPolygon != NULL);
	assert(rgPointFX != NULL);
	assert(wPointCount >= 1);

	for (WORD i = 0; i < wPointCount; i++)
		pPolygon->AddVertex(PointFXToVector2d(&rgPointFX[i]));
}

// SubdivideQSpline - Subdivide a quadratic spline and add the points to a 
//					  polygon.
//
// Parameters:
//	nDepth			- Number of times to recurse.
//	pPolygon		- Polygon to add points on to.
//	pp1, pp2, pp3	- Spline control points.
static 
void SubdivideQSpline(
	Uint nDepth, 
	Polygon2d* pPolygon, 
	Vector2d* pp1, 
	Vector2d* pp2, 
	Vector2d* pp3
)
{
	assert(nDepth >= 0);
	assert(pPolygon != NULL);
	assert(pp1 != NULL);
	assert(pp2 != NULL);
	assert(pp3 != NULL);

	if (nDepth == 0)
		pPolygon->AddVertex(*pp3);
	else
	{
		Vector2d midPoint1 = Vector2d((*pp1 + *pp2)) / 2.0;
		Vector2d midPoint2 = Vector2d((*pp2 + *pp3)) / 2.0;
		Vector2d midPoint3 = Vector2d((midPoint1 + midPoint2)) / 2.0;

		SubdivideQSpline(
			nDepth - 1,
			pPolygon,
			pp1,
			&midPoint1,
			&midPoint3
		);

		SubdivideQSpline(
			nDepth - 1,
			pPolygon,
			&midPoint3,
			&midPoint2,
			pp3
		);
	}
}

// AddQSplineToPolygon - Approximate a series of quadratic spline with line 
//						 segments and add each endpoint to a polygon.
// Parameters:
//	pPolygon		- Polygon to add points to.
//	pStartVertex	- First control point on the spline.
//	rgPointFX		- Array of control points.
//	wPointCount		- Number of points in the array.
static
void AddQSplineToPolygon(
	Polygon2d*	pPolygon, 
	Vector2d*	pStartVertex,
	POINTFX*	rgPointFX, 
	WORD		wPointCount,
	Uint		nSplineQuality
)
{
	assert(pPolygon != NULL);
	assert(rgPointFX != NULL);

	Vector2d p1;
	Vector2d p2;
	Vector2d p3 = *pStartVertex;

	for (WORD i = 0; i < wPointCount - 1; i++)
	{
		p1 = p3;
		p2 = PointFXToVector2d(&rgPointFX[i]);
		if (i == (wPointCount - 2))
			p3 = PointFXToVector2d(&rgPointFX[i + 1]);
		else
		{
			Vector2d a = PointFXToVector2d(&rgPointFX[i]);
			Vector2d b = PointFXToVector2d(&rgPointFX[i+1]);
			p3 = Vector2d(a + b) / 2.0;
		}
		SubdivideQSpline(nSplineQuality, pPolygon, &p1, &p2, &p3);
	}
}

// MergePolygons - Merge a vector of poylgons by joining islands to the outer
//				   polygon.
//
// Parameters:
//	polyVect	- Vector of polygons.  There should be exactly one polygon 
//				  that fully contains the remaining polygons.  No two polygons
//				  should overlap.
//	pChar		- Char in which to add resulting polygons.
//
// Returns:
//	true if the polygon could be merged, false if it was degenerate or if 
//	memory couldn't be allocated.
bool MergePolygons(PolygonList& polyVect, Char *pChar)
{
	if (polyVect.empty())
		return true;

	std::sort(polyVect.begin(), polyVect.end());

	Uint nNumPolys = polyVect.size();

	for(Uint nOutside = 0; nOutside < polyVect.size(); nOutside++)
	{
		if (!polyVect[nOutside].IsCCW())
			polyVect[nOutside].Reverse();

		for(Uint nTest = 0; nTest < polyVect.size();)
		{
			if (nTest != nOutside)
			{
				if (polyVect[nOutside].IsPolygonInside(polyVect[nTest]))
				{
					polyVect[nOutside].MergePolygon(polyVect[nTest]);
					polyVect.erase(polyVect.begin() + nTest);
				}
				else
					nTest++;
			}
			else
				nTest++;
		}
	}
	pChar->m_PolygonList = polyVect;

//	pChar->m_nPolygonCount = polyVect.size();
//	if (pChar->m_nPolygonCount > 0)
//	{
//		pChar->m_pPolygons = new Polygon2d[pChar->m_nPolygonCount];
//		for (Uint i = 0; i < pChar->m_nPolygonCount; i++)
//		{
//			pChar->m_pPolygons[i] = polyVect[i];
//		}
//	}

	return true;
}


// Default constructor
CharOutliner::CharOutliner()
	: m_hDC(NULL), m_hPrevFont(NULL), m_hFont(NULL), m_nSplineQuality(1)
{
}


// Destructor
CharOutliner::~CharOutliner()
{
	if (m_hDC != NULL)
	{
		SelectObject(m_hDC, m_hPrevFont);
		DeleteDC(m_hDC);
	}

	if (m_hFont != NULL)
		DeleteObject(m_hFont);
}

// Init - Initialize an instance of a CharOutliner class.
//
// Parameters:
//	pLogFont	- A Windows LOGFONT structure describing the font to be 
//				  outlined.  THIS MUST BE A TRUETYPE FONT.
//
// Returns:
//	true if the call was successful, false otherwise.
bool CharOutliner::Init(LOGFONT* pLogFont, Uint nSplineQuality)
{
	assert(pLogFont != NULL);

	if ((m_hDC = CreateCompatibleDC(NULL)) == NULL)
		return false;

	if ((m_hFont = CreateFontIndirect(pLogFont)) == NULL)
		return false;
	
	m_hPrevFont = (HFONT)SelectObject(m_hDC, m_hFont);
	m_nSplineQuality = nSplineQuality;

	return true;
}


// GetOutlineBuffer - Get outline data and store in an internal buffer.
//
// Parameters:
//	nChar			- ASCII code of character to retrieve outline data.
//	ppBuffer		- Address of a pointer to the buffer.
//	pdwBufferSize	- Address of the size in bytes of the buffer.
//	pGlyphMetrics	- Metrics for the glyph being outlined.
//
// Returns:
//	true if the call is successful, false otherwise.
bool CharOutliner::GetOutlineBuffer(
	UINT nChar, 
	LPVOID* ppBuffer, 
	DWORD* pdwBufferSize,
	LPGLYPHMETRICS pGlyphMetrics
)
{
	assert(nChar < 256);
	assert(ppBuffer != NULL);
	assert(pdwBufferSize != NULL);

	MAT2 identityMat = {{0, 1}, {0, 0}, {0, 0}, {0, -1}};

	*pdwBufferSize = GetGlyphOutline(
						m_hDC, nChar, GGO_NATIVE, pGlyphMetrics, 
						0, NULL, &identityMat);

	if ((*ppBuffer = new BYTE[*pdwBufferSize]) == NULL)
		return false;

	if (GetGlyphOutline(
			m_hDC, nChar, GGO_NATIVE, pGlyphMetrics, *pdwBufferSize, 
			*ppBuffer, &identityMat) == GDI_ERROR)
		return false;
	
	return true;
}

// GetPolygons - Parse an outline data buffer and construct an array of 
//				 polygon outlines.
//
// Parameters:
//	pBuffer			- Buffer containing outline data.
//	dwBufferSize	- Size of the outline buffer in bytes.
//	pGlyphMetrics	- Metrics for the character.
//	polyVect		- Reference to a vector of polygons.  Each polygon is an 
//					  outline for the character.
void CharOutliner::GetPolygons(
	LPVOID pBuffer, 
	DWORD dwBufferSize, 
	LPGLYPHMETRICS pGlyphMetrics,
	PolygonList& polyVect
)
{
	LPTTPOLYGONHEADER pPolyHeader		= (LPTTPOLYGONHEADER)pBuffer;
	LPTTPOLYGONHEADER pNextPolyHeader	= (LPTTPOLYGONHEADER)((BYTE*)pPolyHeader + pPolyHeader->cb);
	LPTTPOLYCURVE	  pPolyCurve		= (LPTTPOLYCURVE)&pPolyHeader[1];
	BYTE*			  pEndOfBuffer		= (BYTE*)pBuffer + dwBufferSize;
	Polygon2d		  currPolygon;
	Vector2d		  prevVertex;

	while((BYTE*)pPolyHeader < pEndOfBuffer)
	{
		assert(pPolyHeader->dwType == TT_POLYGON_TYPE);

		prevVertex = PointFXToVector2d(&pPolyHeader->pfxStart);
		currPolygon.AddVertex(prevVertex);
		while((BYTE*)pPolyCurve < (BYTE*)pNextPolyHeader)
		{
			assert(
				pPolyCurve->wType == TT_PRIM_LINE || 
				pPolyCurve->wType == TT_PRIM_QSPLINE
			);

			switch(pPolyCurve->wType)
			{
			case TT_PRIM_LINE:
				AddPolyLineToPolygon(
					&currPolygon, 
					pPolyCurve->apfx, 
					pPolyCurve->cpfx
				);
				break;
			case TT_PRIM_QSPLINE:
				AddQSplineToPolygon(
					&currPolygon, 
					&prevVertex, 
					pPolyCurve->apfx, 
					pPolyCurve->cpfx,
					m_nSplineQuality
				);
				break;
			}
			prevVertex = PointFXToVector2d(&pPolyCurve->apfx[pPolyCurve->cpfx - 1]);
			pPolyCurve = (LPTTPOLYCURVE)(((BYTE*)pPolyCurve) + sizeof(TTPOLYCURVE) + ((pPolyCurve->cpfx - 1) * sizeof(POINTFX)));
		}

		pPolyHeader = pNextPolyHeader;
		pNextPolyHeader = (LPTTPOLYGONHEADER)((BYTE*)pPolyHeader + pPolyHeader->cb);
		pPolyCurve = (LPTTPOLYCURVE)&pPolyHeader[1];

		polyVect.push_back(currPolygon);
		currPolygon.RemoveAllVertices();
	}
}

// GetOutline - Get a polygonal outline for a given character.
//
// Parameters:
//	nChar		- Character of outline to retrieve.
//	pPolygon	- A polygon to store the outline.
//
// Returns:
//	true if the call was successful, or false otherwise.
bool CharOutliner::GetOutline(UINT nChar, Char* pChar)
{
	assert(m_hDC != NULL);
	assert(m_hFont != NULL);

	LPVOID		pBuffer = NULL;
	DWORD		dwBufferSize = 0;
	PolygonList	polyVect;
	GLYPHMETRICS glyphMetrics;

	// ### Begin debug
	char szBuf[128];
	sprintf(szBuf, "Outlining %c\n", nChar);
	OutputDebugString(szBuf);
	// ### End debug

	if (!GetOutlineBuffer(nChar, &pBuffer, &dwBufferSize, &glyphMetrics))
		return false;

	GetPolygons(pBuffer, dwBufferSize, &glyphMetrics, polyVect);

	MergePolygons(polyVect, pChar);

	if (pChar->GetPolygonCount() > 0)
		for (Uint i = 0; i < pChar->GetPolygonCount(); i++)
			pChar->GetPolygon(i)->Triangulate(pChar->m_TriangleList);
	pChar->m_BoundingBox.x1	= glyphMetrics.gmptGlyphOrigin.x;
	pChar->m_BoundingBox.y1	= glyphMetrics.gmptGlyphOrigin.y - glyphMetrics.gmBlackBoxY;
	pChar->m_BoundingBox.x2	= glyphMetrics.gmptGlyphOrigin.x + glyphMetrics.gmBlackBoxX;
	pChar->m_BoundingBox.y2	= glyphMetrics.gmptGlyphOrigin.y;
	pChar->m_CellInc.Set(glyphMetrics.gmCellIncX, glyphMetrics.gmCellIncY);

	delete[] pBuffer;
	return true;
}

// GetOutline - Get a polygonal outline for a range of characters.
//
// Parameters:
//	nStartChar	- First character in range of outlines to retrieve.
//	nEndChar	- Last character in range of outlines to retrieve.
//	pPolygon	- An array of polygons to store the outline.
//
// Returns:
//	true if the call was successful, or false otherwise.
bool CharOutliner::GetOutline(UINT nStartChar, UINT nEndChar, Char* pChar)
{
	assert(m_hDC != NULL);
	assert(m_hFont != NULL);
	assert(nStartChar < 256);
	assert(nEndChar < 256);
	assert(pChar != NULL);

	UINT nChar = nStartChar;
	UINT nIndex = 0;

	for(; nChar <= nEndChar; nChar++, nIndex++)
	{
		if (!GetOutline(nChar, &pChar[nIndex]))
			return false;
	}

	return true;
}