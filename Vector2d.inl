//
// Vector2d.inl - Implementation of inline methods of the Vector2d class.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//	

// Constructs a vector from two floats.
inline
Vector2d::Vector2d(float x /* = 0.0 */, float y /* = 0.0 */)
	: m_x(x), m_y(y)
{
}

// Constructs a vector from an array of two floats.
inline
Vector2d::Vector2d(const float v[])
	: m_x(v[0]), m_y(v[1])
{
}
	
// Copy constructor
inline
Vector2d::Vector2d(const Vector2d& v)
{
	m_x = v.m_x;
	m_y = v.m_y;
}
	
// Assignment operator.
inline
const Vector2d& Vector2d::operator=(const Vector2d& v)
{
	m_x = v.m_x;
	m_y = v.m_y;

	return *this;
}

// Addition operator.
inline
const Vector2d Vector2d::operator+(const Vector2d& v)
{
	return Vector2d(m_x + v.m_x, m_y + v.m_y);
}

// Subtraction operator.
inline
const Vector2d Vector2d::operator-(const Vector2d& v)
{
	return Vector2d(m_x - v.m_x, m_y - v.m_y);
}

// Division by scalar operator.
inline
const Vector2d Vector2d::operator/(float fDivisor)
{
	assert(fDivisor != 0.0);
	return Vector2d(m_x / fDivisor, m_y / fDivisor);
}

inline
const Vector2d Vector2d::operator*(float fMult)
{
	return Vector2d(m_x * fMult, m_y * fMult);
}

inline
bool Vector2d::operator==(const Vector2d& v)
{
	return (fabs(v.m_x - m_x) < FLOAT_TOLERANCE) &&
		(fabs(v.m_y - m_y) < FLOAT_TOLERANCE);
}

inline
bool Vector2d::operator!=(const Vector2d& v)
{
	return !operator==(v);
}


// Set x and y components.
inline
void Vector2d::Set(float x, float y)
{
	m_x = x;
	m_y = y;
}

// Returns the dot product of this vector with another.
inline
float Vector2d::Dot(const Vector2d& v) const
{
	return (m_x * v.m_x) + (m_y * v.m_y);
}

// Returns the z component of the cross product of this vector and another.
inline
float Vector2d::Cross(const Vector2d& v) const
{
	return (m_x * v.m_y - m_y * v.m_x);
}


// Returns the magnitude of this vector.
inline
float Vector2d::Length() const
{
	return (float)sqrt((m_x * m_x) + (m_y * m_y));
}

// Returns the distance between this vector and another.
inline
float Vector2d::Distance(Vector2d& v) const
{
	float dx = v.m_x - m_x;
	float dy = v.m_y - m_y;

	return (float)sqrt((dx * dx) + (dy * dy));
}

// Unitize this vector.
inline
void Vector2d::Unitize()
{
	float fLength = Length();

	if (fLength != 0.0)
	{
		m_x /= fLength;
		m_y /= fLength;
	}
}

inline
float Vector2d::Angle(const Vector2d& v) const
{
	float fTheta1 = (float)atan2(m_y, m_x);
	float fTheta2 = (float)atan2(v.m_y, v.m_x);
	float fDelta = fTheta2 - fTheta1;

	while (fDelta > M_PI)
		fDelta -= (float)(2 * M_PI);

	while (fDelta < -M_PI)
		fDelta += (float)(2 * M_PI);

   return fDelta;
}
