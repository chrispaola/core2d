//
// Polygon2d.cpp - Implementation of methods of the Polygon2d class.
//
// Copyright 2001 - 2013 Chris Paola
//
// Licensed under the apache license, version 2.0 (the "license");
// you may not use this file except in compliance with the license.
// You may obtain a copy of the license at
//
//    http://www.apache.org/licenses/license-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the license is distributed on an "as is" basis,
// without warranties or conditions of any kind, either express or implied.
// See the license for the specific language governing permissions and
// limitations under the license.
//

#include <windows.h> //DELME, for OutputDebugString ONLY
#include <stdio.h>	// ditto
#include <assert.h>
#include <vector>
#include "Polygon2d.h"
#include "Triangle.h"

// Returns the x coordinate of the left most vertex.  This method must
// only be called on a polygon with atleast one vertex.
float Polygon2d::GetLeftness() const
{
	assert(GetVertexCount() >= 1);

	float fLeftness = FLT_MAX;
	Uint nVertexCount = GetVertexCount();

	for (Uint i = 0; i < nVertexCount; i++)
		if (GetVertex(i).m_x < fLeftness)
			fLeftness = GetVertex(i).m_x;

	return fLeftness;
}

bool Polygon2d::IsPointInside(const Vector2d& point) const
{
	assert(GetVertexCount() >= 3);

	float fAngle = 0.0;

	for (Uint i = 0; i < GetVertexCount(); i++)
	{
		Vector2d v0 = Vector2d(GetVertex(i).m_x - point.m_x, GetVertex(i).m_y - point.m_y);
		Vector2d v1 = Vector2d(GetVertex(i + 1).m_x - point.m_x, GetVertex(i + 1).m_y - point.m_y);

		fAngle += v0.Angle(v1);
	}

	return (fabs(fAngle) >= M_PI);
}


bool Polygon2d::IsPolygonInside(const Polygon2d& polygon) const
{
	if (GetVertexCount() >= 3)
	{
		for(Uint i = 0; i < polygon.GetVertexCount(); i++)
		{
			if (!IsPointInside(polygon.GetVertex(i)))
				return false;
		}
		return true;
	}

	return false;	
}

// Find the first ear in the polygon.
Uint Polygon2d::GetNextEar() const
{
	Uint nIndex = 0;

	while(nIndex < GetVertexCount())
	{
		bool bIsEar = true;		// Start optimistically

		// Three consecutive vertices vi-1, vi, vi+1 form an ear iff
		// 1. vi is a convex vertex
		// 2. The closure of the triangle vi-1, vi, vi+1 does not contain a
		//    reflex vertex (except vi-1, vi+1)
		if (IsVertexConvex(nIndex))
		{
			Vector2d a(GetVertex(nIndex - 1));
			Vector2d b(GetVertex(nIndex));
			Vector2d c(GetVertex(nIndex + 1));
			Triangle tri(a, b, c);

			// Search for a reflex vertex inside the triangle
			for (Uint i = 0; i < GetVertexCount(); i++)
			{
				// Skip over vi-1, vi, vi+1
				if(
					i == GetIndexInBounds(nIndex - 1) || 
					i == GetIndexInBounds(nIndex) || 
					i == GetIndexInBounds(nIndex + 1)
				)
					continue;

				if (!IsVertexConvex(i))
				{
					if (tri.IsPointInside(GetVertex(i)))
					{
						bIsEar = false;
						break;
					}
				}
			}
			if (bIsEar)
				return nIndex;
		}
		nIndex++;
	}

	return -1;
}

// www.wireplay.com.au
// www.redfaction.com
void Polygon2d::Triangulate(TriangleList& triList) const
{
	Polygon2d poly = *this;			// Temporary working polygon
	bool bFailed = false;

	assert(GetVertexCount() >= 3);

	if (!IsCCW())
		poly.Reverse();

	poly.RemoveDuplicates();

	while(poly.GetVertexCount() > 3)
	{
		int			nEarIndex = poly.GetNextEar();
		Triangle	tmpTri;

		if (nEarIndex < 0)
		{
			bFailed = true;
			break;
		}

		tmpTri.Set(
			poly.GetVertex(nEarIndex - 1),
			poly.GetVertex(nEarIndex),
			poly.GetVertex(nEarIndex + 1)
		);

		poly.RemoveVertex(nEarIndex);

		triList.push_back(tmpTri);
	}

//	assert(poly.GetVertexCount() == 3);

	if (!bFailed)
		triList.push_back(
			Triangle(poly.GetVertex(0), poly.GetVertex(1), poly.GetVertex(2))
		);
	else
	{
		for (Uint i = 0; i < poly.GetVertexCount(); i++)
		{
			char szBuf[80];
			sprintf(
				szBuf, 
				"poly[%2i] = (%2.4f, %2.4f)\n", 
				i, 
				poly.GetVertex(i).m_x,
				poly.GetVertex(i).m_y
			);
			OutputDebugString(szBuf);
		}
	}
}

float Polygon2d::GetArea() const
{
	float fArea	= 0.0;
	Uint i		= 0;
	Uint nVertexCount = GetVertexCount();

	for (; i < nVertexCount; i++)
	{
		Vector2d prevVertex = GetVertex(i - 1);
		Vector2d currVertex = GetVertex(i);
		Vector2d nextVertex = GetVertex(i + 1);

		fArea += currVertex.m_x * (nextVertex.m_y - prevVertex.m_y);
	}
	return fArea / 2.0;
}

void Polygon2d::Reverse()
{
	List tmpList;
	List::reverse_iterator iter = m_List.rbegin();

	while(iter != m_List.rend())
	{
		tmpList.push_back(*iter);
		iter++;
	}

	m_List = tmpList;
}

void Polygon2d::MergePolygon(Polygon2d& polygon)
{
	if (polygon.IsCCW())
		polygon.Reverse();

	Uint nLeftIndex = -1;
	float fLeftest = FLT_MAX;

	for (Uint i = 0; i < polygon.GetVertexCount(); i++)
	{
		if (polygon.GetVertex(i).m_x < fLeftest)
		{
			fLeftest = polygon.GetVertex(i).m_x;
			nLeftIndex = i;
		}
	}
	assert(nLeftIndex != -1);

	Vector2d inVector = polygon.GetVertex(nLeftIndex);
	float fClosest = FLT_MAX;
	Uint nClosestIndex = 0xffffffff;

	for (Uint i = 0; i < GetVertexCount(); i++)
	{
		Vector2d outVector = GetVertex(i);

		if (
			outVector.Distance(inVector) < fClosest && 
			outVector.m_x < inVector.m_x
		)
		{
			nClosestIndex = i;
			fClosest = outVector.Distance(inVector);
		}
	}

	// nClosestIndex is an index to the outside poly in pos where to merge
	// nLeftIndex is an index to the inside poly where to merge

	Uint pos = nClosestIndex + 1;
	for (Uint i = 0; i <= polygon.GetVertexCount(); i++)
		m_List.insert(
			m_List.begin() + pos++, 
			polygon.GetVertex(i + nLeftIndex)
		);
	m_List.insert(m_List.begin() + pos, GetVertex(nClosestIndex));
}

void Polygon2d::RemoveDuplicates()
{
	Uint i;

	for (i = 0; i < GetVertexCount(); i++)
	{
		Vector2d v1 = GetVertex(i);
		Vector2d v2 = GetVertex(i + 1);

		if (v1.m_x == v2.m_x && v1.m_y == v2.m_y)
			RemoveVertex(i + 1);
	}
}

void Polygon2d::SetVertex(int nIndex, const Vector2d& vector)
{
	Vector2d& v = m_List.at(GetIndexInBounds(nIndex));

	v = vector;
}


void Polygon2d::GetBoundingBox(float& x0, float& y0, float& x1, float& y1)
{
	x0 = FLT_MAX;
	y0 = FLT_MAX;
	x1 = -FLT_MAX;
	y1 = -FLT_MAX;

	for (Uint i = 0; i < GetVertexCount(); i++)
	{
		Vector2d v = GetVertex(i);

		if (v.m_x < x0)
			x0 = v.m_x;

		if (v.m_y < y0)
			y0 = v.m_y;

		if (x1 < v.m_x)
			x1 = v.m_x;

		if (y1 < v.m_y)
			y1 = v.m_y;
	}
}

void Polygon2d::Translate(float dx, float dy)
{
	for (Uint i = 0; i < GetVertexCount(); i++)
	{
		Vector2d v = GetVertex(i);

		v.m_x += dx;
		v.m_y += dy;

		SetVertex(i, v);
	}
}